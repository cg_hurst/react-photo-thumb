const PageHelper = (itemCount, itemsPerPage, currentPage, pagesToShow) => {

    let totalPages = Math.ceil(itemCount/itemsPerPage);

    let offset = Math.floor(pagesToShow / 2);

    let firstPage = currentPage - offset;
    if (firstPage < 1) firstPage = 1;

    let lastPage = firstPage + pagesToShow - 1;
    if (lastPage > totalPages) lastPage = totalPages;
    
    return {firstPage: firstPage, lastPage: lastPage};
}

export default PageHelper;