import {
    ACTION_REQUEST_PHOTOS,
    ACTION_REQUEST_PHOTOS_SUCCESS,
    ACTION_REQUEST_PHOTOS_ERROR,
    ACTION_SET_CURRENT_PAGE
} from './constants'
  
  const initialState = {
    photos: [],
    isPending: true,
    currentPage: 1
  }
  
  export const galleryReducer = (state=initialState, action={}) => {
    switch (action.type) {
      case ACTION_REQUEST_PHOTOS:
        return Object.assign({}, state, {isPending: true});
      case ACTION_REQUEST_PHOTOS_SUCCESS:
        return Object.assign({}, state, {photos: action.payload, isPending: false});
      case ACTION_REQUEST_PHOTOS_ERROR:
        return Object.assign({}, state, {error: action.payload});
      case ACTION_SET_CURRENT_PAGE:
        return Object.assign({}, state, {currentPage: action.payload});
      default:
        return state;
    }
  }