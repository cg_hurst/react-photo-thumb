import {
    ACTION_REQUEST_PHOTOS,
    ACTION_REQUEST_PHOTOS_SUCCESS,
    ACTION_REQUEST_PHOTOS_ERROR,
    ACTION_SET_CURRENT_PAGE
} from './constants'

export const requestPhotos = () => (dispatch) => {
    dispatch({ type: ACTION_REQUEST_PHOTOS })
    fetch('https://jsonplaceholder.typicode.com/albums/1/photos')
    .then(response => response.json())
      .then(data => dispatch({ type: ACTION_REQUEST_PHOTOS_SUCCESS, payload: data }))
      .catch(error => dispatch({ type: ACTION_REQUEST_PHOTOS_ERROR, payload: error }))
}

export const setCurrentPage = (page) => {
    console.log(page);
    return ({ type: ACTION_SET_CURRENT_PAGE, payload: page })
}