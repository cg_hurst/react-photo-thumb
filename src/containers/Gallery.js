import React from 'react';
import { connect } from 'react-redux';
import { requestPhotos, setCurrentPage } from '../actions';
import './Gallery.css';
import Photo from '../components/Photo';
import Pagination from '../components/Pagination';

// parameter state comes from index.js provider store state(rootReducers)
const mapStateToProps = (state) => {
  return {
    photos: state.photos,
    isPending: state.isPending,
    currentPage: state.currentPage
  }
}

// dispatch the DOM changes to call an action. note mapStateToProps returns object, mapDispatchToProps returns function
// the function returns an object then uses connect to change the data from redecers.
const mapDispatchToProps = (dispatch) => {
  return {
    onRequestPhotos: () => dispatch(requestPhotos()),
    onPageChange: (event) => dispatch(setCurrentPage(parseInt(event.target.getAttribute('data-page')))) 
  }
}


class Gallery extends React.Component {
  componentDidMount() {
    this.props.onRequestPhotos();
  }

  render() {

    const currentPage = this.props.currentPage;

    const firstItem = ((currentPage - 1) * 20);
    const lastItem = firstItem + 19;

    const pagePhotos = this.props.photos.slice(firstItem,lastItem);

    return (
      <div className="Gallery">
        <div>
        {
          pagePhotos.map((photo, idx) => {
            return (
              <Photo key={photo.id}
              title={photo.title}
              url={photo.url}
              thumbnailUrl={photo.thumbnailUrl} 
              />
          )})
        }
        </div>
        <Pagination 
          totalItems={this.props.photos.length}
          itemsPerPage={20}
          pagesToShow={5}
          currentPage={currentPage}
          setCurrentPage={this.props.onPageChange}
         />
      </div>
    );
  }
}

// action done from mapDispatchToProps will channge state from mapStateToProps
export default connect(mapStateToProps, mapDispatchToProps)(Gallery)