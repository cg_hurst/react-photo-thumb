import React from 'react';
import PageHelper from '../helpers/PageHelper';

class Pagination extends React.Component{

    render(){
        const {totalItems, currentPage, itemsPerPage, pagesToShow, setCurrentPage} = this.props;
        const pages = [];
        const {firstPage, lastPage} = PageHelper(totalItems, itemsPerPage, currentPage, pagesToShow);

        for (let i = firstPage; i <= lastPage; i++){
            let sel = i === currentPage ? " bg-white" : " bg-transparent";
            pages.push(<button className={"ph3 mh1 ba black" + sel} data-page={i} onClick={setCurrentPage}>{i}</button>);
        }

        return (
            <div>
                {pages}
            </div>
        );
    }
}

export default Pagination;