import React from 'react';

class Photo extends React.Component {
  render() {
    return (
      <div className="tc grow bg-white br3 pb3 pa1 ma2 dib shadow-5">
          <a className="db link dim tc" href={this.props.url} target="_blank" rel="noopener noreferrer">
            <img className="w-100 br3 db black-10" alt={this.props.title} src={this.props.thumbnailUrl} />
          </a>
        </div>
    );
  }
}

export default Photo;