import React from 'react';
import ReactDOM from 'react-dom';
import { createStore, applyMiddleware } from 'redux';
import { Provider } from 'react-redux';
import thunkMiddleware from 'redux-thunk';

import {galleryReducer} from './reducer'

import Gallery from './containers/Gallery';
import * as serviceWorker from './serviceWorker';
import './index.css';

const store = createStore(galleryReducer, applyMiddleware(thunkMiddleware))

ReactDOM.render(
  <Provider store={store}>
    <Gallery />
  </Provider>,
  document.getElementById('root')
);
serviceWorker.unregister();
